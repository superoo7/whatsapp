var webpack = require('webpack');
var path = require('path');

var ExtractTextPlugin = require('extract-text-webpack-plugin');

var HtmlWebpackPlugin = require('html-webpack-plugin');

var OfflinePlugin = require('offline-plugin');


const VENDOR_LIBS = [
	"react-redirect", "react-modal", "clipboard"
];

module.exports = {
  entry: {
    bundle: './src/index.js',
    vendor: VENDOR_LIBS
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].[chunkhash].js'
  },
  devServer: {
    historyApiFallback: true
  },
  module: {
  	rules: [
  		{
  			use: 'babel-loader',
  			test: /\.js$/,
  			exclude: /node_modules/
  		},
  		{
  			test: /\.css$/,
        use: ExtractTextPlugin.extract({
          use: "css-loader?modules,localIdentName='[name]-[local]-[hash:base64:6]',camelCase"
        })
  		},
	    {
	      test: /\.(jpe?g|png|gif|svg)$/,
	      use: [
	        {
	            loader: 'url-loader',
	            options: { limit: 40000, prefix: 'file-loader' }
	        },
	        'image-webpack-loader']
	    }

  	]
  },

  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'manifest']
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    }),
    new ExtractTextPlugin('style.css'),
    new webpack.DefinePlugin({
      'process.env': {
				NODE_ENV: JSON.stringify('production')
			}
    }),
		new webpack.optimize.UglifyJsPlugin(),
		new OfflinePlugin()

	]
};
