import React from 'react';
import {
	Route,
	Switch
} from 'react-router-dom';

import Home from './components/Home/Home';
import Whatsapp from './components/Whatsapp/Whatsapp';
import NotFound from './components/NotFound/NotFound';

export default class Routes extends React.Component {
	render() {
		return(

				<div>
		          <Switch>
		            <Route exact path='/' component={Home} />
		            <Route path='/ws:ws' component={({match}) => {
									const ws = match.params.ws;
									return <Whatsapp ws={ws} />
								}} />
								<Route component={NotFound} />
		            </Switch>
	        </div>

		);
	}
}
