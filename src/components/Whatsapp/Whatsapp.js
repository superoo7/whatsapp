import React, { Component } from 'react';
import ReactRedirect from 'react-redirect';
import Clipboard from 'Clipboard';

import container from '../Shared/Container.css';
import styles from './Whatsapp.css';

class Whatsapp extends Component {
  constructor (props) {
    super(props);
    const number = props.ws;
    if (!!number) {
      this.URL = `whatsapp://send?text=Hi&phone=${number}&abid=${number}`;
      this.state = {
        ws: number,
        URL: `whatsapp://send?text=Hi&phone=${number}&abid=${number}`,
        justCopied: false
      };
    } else {
      this.state = {
        ws: "",
        URL: "",
        justCopied: false
      };
    }
    this.changeNumber = this.changeNumber.bind(this);
  }
  componentDidMount() {
    this.clipboard = new Clipboard(this.refs.copy);

    this.clipboard.on('success', () => {
        alert('copied');
        this.setState({ justCopied: true });
        setTimeout(() => {
            this.setState({justCopied: false});
        }, 1000);
    }).on('error', () => {
        alert('unable to copy');
    })
}

  componentWillUnmount() {
      this.clipboard.destroy();
  }


  changeNumber(e) {
    let phoneNo = e.target.value
    this.setState({
      ws: phoneNo,
      URL: `whatsapp://send?text=Hi&phone=${phoneNo}&abid=${phoneNo}`
     });
  }


  render () {
    return (
      <div className={container.container}>
        <input type="tel" value={this.state.ws} onChange={this.changeNumber} placeholder="insert phone number with country code" />
        {this.URL? <ReactRedirect location={this.URL} /> : undefined}
        <input className={styles.copyInput} value={this.state.URL} disabled />
        <div className={styles.buttonContainer}>
          <a className={styles.link} href={this.state.URL}><button className={styles.open}>Open</button></a>
          <button className={styles.copy} ref="copy" data-clipboard-text={`https://whatsapp.surge.sh/ws${this.state.ws}`}>
            {this.state.justCopied? 'Copied' : 'Copy'}
          </button>
        </div>
      </div>
    );
  }
}

export default Whatsapp;
