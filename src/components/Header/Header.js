import React from 'react';
import styles from './Header.css';

import { Link } from 'react-router-dom';

import Modal from 'react-modal';

export default class Header extends React.Component {
	constructor(props) {
		super(props);
		this.state={
			isOpen: false
		};
		this.openModal = this.openModal.bind(this);
	}

	openModal() {
		this.setState({
			isOpen: true
		});
	}

	render() {
		return(
			<div>
			<header className={styles.header}>
				<div className={styles.container}>
					<h1 className={styles.title}>
						<Link to='/'>Util</Link>
					</h1>
					<ul className={styles.list}>
						<li className={styles.listItem}>Whatsapp</li>
					</ul>
				</div>
			</header>
			<div className={styles.wrap}>
				<button
					className={styles.button}
					onClick={this.openModal}>
					?
				</button>
			</div>
			<Modal
                isOpen={this.state.isOpen}
                contentLabel="Inquiry"
                onRequestClose={()=>this.setState({isOpen: false})}
                className={styles.boxedView}
                overlayClassName={styles.boxedViewModal}
            >
            	<div className={styles.modal}>
            		<h1 className={styles.modalTitle}>
									Util website build by <a href="http://superoo7.com">superoo7</a>
								</h1>
            		<p className={styles.modalContent}>
									1. easily message somebody without adding them into your contact. <br />
									2. share link to others to whatsapp you.
								</p>
            	</div>
            </Modal>
            </div>
		);
	}
}
