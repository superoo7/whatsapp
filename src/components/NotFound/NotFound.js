import React from 'react';
import superoo7 from '../../../assets/superoo7.png';

import styles from "./NotFound.css";
import container from "../Shared/Container.css";

const NotFound = () => (
  <div className={container.container}>
    <img className={styles.superoo7} src={superoo7} alt="superoo7.png"/>
    <p className={styles.title}>You came to the wrong page...</p>
  </div>
);

export default NotFound;
