import React from 'react';
import ReactDOM, {render} from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {install} from 'offline-plugin/runtime';

// CSS
import './Base.css';
import './index.css';

// Route
import Routes from './Routes';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';

const App = () => (
		<BrowserRouter>
			<div>
				<Header />
				<Routes />
				<Footer />
			</div>
		</BrowserRouter>
);

render(<App />, document.getElementById('app'));

if (process.env.NODE_ENV === 'production') {
	install();
}
